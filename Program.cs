﻿using System;

class Program
{
   static void Main()
   {
      Console.Write("How many employees will be registered? "); int amountEmployee = int.Parse(Console.ReadLine());
      Employee[] array = new Employee[amountEmployee];
      
      for (int i = 0; i < amountEmployee; i++)
      {
         Console.Write("ID: "); int id = int.Parse(Console.ReadLine());
         Console.Write("Name: "); string name = Console.ReadLine();
         Console.Write("Salary: "); double salary = double.Parse(Console.ReadLine());

         array[i].id = id;
         array[i].name = name;
         array[i].salary = salary;

         Console.WriteLine();
      }

      for (int i = 0; i < array.Length; i++)
      {
         if (i != array.Length - 1) {
            int tmpID = array[i++].id;

            if (array[i].id == tmpID) {
               Console.WriteLine("ERROR: Repeated ID found!");
               return;
            }
         }
      }

      Console.Write("Enter the employee id that will have salary increase: "); int idToIncrease = int.Parse(Console.ReadLine());

      bool idFound = false;

      for (int i = 0; i < array.Length; i++)
      {
         if (idToIncrease == array[i].id) {
            idFound = true;
            Console.Write("Enter the percentage: "); double percentage = double.Parse(Console.ReadLine());
            array[i].IncreaseSalary(percentage);

            Console.WriteLine();
            Console.WriteLine("Updated list of employees:");
            for (int j = 0; j < array.Length; j++) {
               Console.WriteLine($"{array[j].id}, {array[j].name}, {array[j].salary}");
            }
         }
      }

      if (idFound == false)
      {
         Console.WriteLine("This id does not exist!");
      }
   }
}

struct Employee()
{
   public int id;
   public string name;
   public double salary;

   public void IncreaseSalary(double percentage)
   {
      salary = salary + (salary /10);
   }
}